<?php

/**
 * Definition of Drupal\config\Plugin\Type\ConfigEntityListingManager.
 */

namespace Drupal\config\Plugin\Type;

use Drupal\Component\Plugin\PluginManagerBase;
use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Plugin\Discovery\AnnotatedClassDiscovery;
use Drupal\Core\Plugin\Discovery\CacheDecorator;
use Drupal\Component\Plugin\Exception\PluginException;

/**
 * Plugin manager for configuration entity listings.
 */
class ConfigEntityListingManager extends PluginManagerBase {

  public function __construct() {
    $this->discovery = new CacheDecorator(new AnnotatedClassDiscovery('config', 'listing'), 'config:listing', 'cache');
    $this->factory = new DefaultFactory($this);
  }

}
