<?php

/**
 * @file
 * Definition of Drupal\entity\ContentEntityInterface.
 */

namespace Drupal\entity;

/**
 * Defines a common interface for all content entity objects.
 */
interface ContentEntityInterface extends EntityInterface {

}
